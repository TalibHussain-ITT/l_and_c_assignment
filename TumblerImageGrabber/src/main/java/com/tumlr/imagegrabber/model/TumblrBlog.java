package com.tumlr.imagegrabber.model;

public class TumblrBlog {

	private String title;
	private String name;
	private String descritpion;
	private int postCount;

	public TumblrBlog(String title, String name, String descritpion, int postCount) {
		super();
		this.title = title;
		this.name = name;
		this.descritpion = descritpion;
		this.postCount = postCount;
	}

	public String getTitle() {
		return title;
	}

	public String getName() {
		return name;
	}

	public String getDescritpion() {
		return descritpion;
	}

	public int getPostCount() {
		return postCount;
	}
}
