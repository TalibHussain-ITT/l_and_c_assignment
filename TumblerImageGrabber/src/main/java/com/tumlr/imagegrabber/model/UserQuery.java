package com.tumlr.imagegrabber.model;

public class UserQuery {
	private int startPost;
	private int endPost;
	private String blogName;

	public UserQuery() {
		super();
	}

	public UserQuery(int startPost, int endPost, String blogName) {
		super();
		this.startPost = startPost;
		this.endPost = endPost;
		this.blogName = blogName;
	}

	public int getStartPost() {
		return startPost;
	}

	public int getEndPost() {
		return endPost;
	}

	public String getBlogName() {
		return blogName;
	}

	public UserQuery setBlogName(String blogName) {
		this.blogName = blogName;
		return this;
	}

	public UserQuery setStartPost(int startPost) {
		this.startPost = startPost;
		return this;
	}

	public UserQuery setEndPost(int endPost) {
		this.endPost = endPost;
		return this;
	}
}
