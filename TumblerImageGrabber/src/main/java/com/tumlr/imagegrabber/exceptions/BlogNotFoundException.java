package com.tumlr.imagegrabber.exceptions;

import java.io.FileNotFoundException;

public class BlogNotFoundException extends FileNotFoundException {

	private static final long serialVersionUID = 7692079379559942665L;

	public BlogNotFoundException() {
		super();
	}

	public BlogNotFoundException(String message) {
		super(message);
	}
}
