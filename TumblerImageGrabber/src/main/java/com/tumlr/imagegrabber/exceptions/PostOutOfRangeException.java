package com.tumlr.imagegrabber.exceptions;

public class PostOutOfRangeException extends RuntimeException {

	private static final long serialVersionUID = -7217270033855019944L;

	public PostOutOfRangeException() {
		super();
	}

	public PostOutOfRangeException(String message, Throwable cause) {
		super(message, cause);
	}

	public PostOutOfRangeException(String message) {
		super(message);
	}
}
