package com.tumlr.imagegrabber.service;

import java.io.BufferedReader;
import java.io.IOException;

public class ResponseReader {

	public ResponseReader() {
		super();
	}

	public String readResponsedData(BufferedReader dataReader) throws IOException {
		StringBuilder responseData = new StringBuilder();
		String currentLine = dataReader.readLine();
		while (currentLine != null) {
			responseData.append(currentLine);
			currentLine = dataReader.readLine(); // moving to next line.
		}
		return responseData.toString();
	}
}
