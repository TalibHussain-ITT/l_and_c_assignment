package com.tumlr.imagegrabber.service;

import java.io.IOException;

import com.tumlr.imagegrabber.model.UserQuery;
import com.tumlr.imagegrabber.util.NoiseRemover;

public class FetchTumblr {

	public FetchTumblr() {
		super();
	}
	public String fetchBlogJson(UserQuery query) throws IOException {
		String url = new TumblrURL().generateUrl(query);
		String responseContent = new TumblrRequest().requestData(url);
		
		// We have to remove Noise characters to extract JSON Data
		String jsonData = NoiseRemover.removeNoise(responseContent); 
		
		return jsonData;
	}
}
