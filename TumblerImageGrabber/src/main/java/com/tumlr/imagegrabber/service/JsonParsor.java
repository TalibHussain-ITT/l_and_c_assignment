package com.tumlr.imagegrabber.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tumlr.imagegrabber.model.TumblrBlog;

public class JsonParsor {

	private static final String PHOTO_URL = "photo-url-1280";
	private static final String POSTS = "posts";
	private static final String PHOTOS = "photos";
	private static final String TUMBLE_LOG = "tumblelog";
	private static final String TOTAL_POSTS = "posts-total";
	private static final String TITLE = "title";
	private static final String DESCRIPTION = "description";
	private static final String NAME = "name";
	private JSONObject blogJson;

	public JsonParsor(String blogData) throws JSONException {
		super();
		this.blogJson = new JSONObject(blogData);
	}

	public TumblrBlog parseTumblrBlog() throws JSONException {
		JSONObject blogData = this.blogJson.getJSONObject(TUMBLE_LOG);
		int postCount = this.blogJson.getInt(TOTAL_POSTS);
		String title = blogData.getString(TITLE);
		String description = blogData.getString(DESCRIPTION);
		String name = blogData.getString(NAME);

		return new TumblrBlog(title, name, description, postCount);
	}

	private boolean hasMoreThanOnePhoto(JSONArray photos) {
		if (photos != null && photos.length() > 0) {
			return true;
		}
		return false;
	}

	private JSONArray parsePhotoLinks(JSONArray photos) throws JSONException {
		JSONArray photoList = new JSONArray();
		for (int photoIndex = 0; photoIndex < photos.length(); photoIndex++) {

			// Parsing photo URL from photos array
			String photoLink = photos.getJSONObject(photoIndex).getString(PHOTO_URL);
			photoList.put(photoLink);
		}
		return photoList;
	}

	public JSONArray parseImageLinks(JSONArray postImageList) throws JSONException {
		JSONArray postsJson = this.blogJson.getJSONArray(POSTS);

		for (int postIndex = 0; postIndex < postsJson.length(); postIndex++) {
			JSONArray photos = postsJson.getJSONObject(postIndex).getJSONArray(PHOTOS);

			if (hasMoreThanOnePhoto(photos)) {
				postImageList.put(parsePhotoLinks(photos));
			} else {
				String photoLink = postsJson.getJSONObject(postIndex).getString(PHOTO_URL);
				JSONArray photoList = new JSONArray().put(photoLink);
				postImageList.put(photoList);
			}
		}
		return postImageList;
	}
}
