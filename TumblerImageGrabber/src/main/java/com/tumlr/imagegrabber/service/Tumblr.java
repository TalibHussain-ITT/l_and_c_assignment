package com.tumlr.imagegrabber.service;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;

import com.tumlr.imagegrabber.model.TumblrBlog;
import com.tumlr.imagegrabber.model.UserQuery;
import com.tumlr.imagegrabber.util.NoiseRemover;

public class Tumblr {
	private static final int MAX_LIMIT_TO_FETCH_POST = 50;

	public Tumblr() {
		super();
	}

	public String fetchBlogJson(UserQuery query) throws IOException {
		String url = new TumblrURL().generateUrl(query);
		String responseContent = new TumblrRequest().requestData(url);

		// We have to remove Noise characters to extract JSON Data
		return NoiseRemover.removeNoise(responseContent);
	}

	public TumblrBlog fetchBlogDetails(String blogName) throws IOException, JSONException {

		// Initially we only need blog details.
		UserQuery query = new UserQuery().setBlogName(blogName);

		String jsonData = fetchBlogJson(query);
		JsonParsor jsonParsor = new JsonParsor(jsonData);
		return jsonParsor.parseTumblrBlog();
	}

	public JSONArray fetchPostPhotos(UserQuery query) throws IOException, JSONException {
		JSONArray postList = new JSONArray();
		int startPost = query.getStartPost();
		int endPost = query.getEndPost();
		int postsFetchCount = endPost - startPost;
		int remainingPostsToFetch = postsFetchCount;

		// If endPost number is greater then than MAX_LIMIT_TO_FETCH_POST then Loop
		do {
			if (remainingPostsToFetch < MAX_LIMIT_TO_FETCH_POST) {
				query.setEndPost(query.getStartPost() + remainingPostsToFetch);
				remainingPostsToFetch = 0;
			} else {
				remainingPostsToFetch -= MAX_LIMIT_TO_FETCH_POST;
				query.setEndPost(query.getStartPost() + MAX_LIMIT_TO_FETCH_POST);
			}

			String jsonData = new Tumblr().fetchBlogJson(query);
			JsonParsor jsonParsor = new JsonParsor(jsonData);
			postList = jsonParsor.parseImageLinks(postList);

			query.setStartPost(query.getStartPost() + MAX_LIMIT_TO_FETCH_POST);

		} while (remainingPostsToFetch > 0);

		return postList;
	}
}
