package com.tumlr.imagegrabber.service;

import com.tumlr.imagegrabber.model.UserQuery;

public class TumblrURL {
	private static final String URL = "https://%s.tumblr.com/api/read/json?type=photo&num=%d&start=%d";

	public TumblrURL() {
		super();
	}

	public String generateUrl(UserQuery query) {

		int startPost = query.getStartPost();
		int endPost = query.getEndPost();
		int numberOfPostToFetch = endPost - startPost;

		// Adding Parameters in Api URL.
		return String.format(URL, query.getBlogName(), numberOfPostToFetch, startPost);
	}
}
