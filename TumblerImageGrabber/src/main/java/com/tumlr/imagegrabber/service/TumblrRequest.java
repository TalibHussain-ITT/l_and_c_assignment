	package com.tumlr.imagegrabber.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import com.tumlr.imagegrabber.exceptions.BlogNotFoundException;

public class TumblrRequest {

	private static final String BLOG_DOES_NOT_EXIST_MSG = "The blog is not available";

	public TumblrRequest() {
		super();
	}

	private String readResponsedData(BufferedReader dataReader) throws IOException {
		StringBuilder responseData = new StringBuilder();
		String currentLine = dataReader.readLine();

		while (currentLine != null) {
			responseData.append(currentLine);
			currentLine = dataReader.readLine(); // moving to next line.
		}
		return responseData.toString();
	}

	public String requestData(String url) throws IOException {
		String requestData = null;
		try {
			InputStream rawDataStream = new URL(url).openStream();
			BufferedReader dataReader = new BufferedReader(
					new InputStreamReader(rawDataStream, Charset.forName("UTF-8")));
			requestData = readResponsedData(dataReader);

		} catch (FileNotFoundException exception) {

			throw new BlogNotFoundException(BLOG_DOES_NOT_EXIST_MSG);
		}
		return requestData;
	}
}
