package com.tumlr.imagegrabber.service;

import java.io.IOException;

import org.json.JSONArray;

import com.tumlr.imagegrabber.model.UserQuery;

public class ImageGrabber {

	private static final int MAX_LIMIT_TO_FETCH_POST = 50;

	public ImageGrabber() {
	}

	public JSONArray fetchPostPhotos(UserQuery query) throws IOException {
		JSONArray postList = new JSONArray();
		int postsFetchCount = query.getEndPost() - query.getStartPost();
		int remainingPostsToFetch = postsFetchCount;
		
		 query.setEndPost(MAX_LIMIT_TO_FETCH_POST);
		// if endPost number is greater then than MAX_LIMIT_TO_FETCH_POST then Loop
		do {
			if (remainingPostsToFetch < MAX_LIMIT_TO_FETCH_POST) {
				query.setEndPost(query.getStartPost() + remainingPostsToFetch);
				remainingPostsToFetch = 0;
			} else {
				remainingPostsToFetch -= MAX_LIMIT_TO_FETCH_POST;
			}
			String jsonData = new FetchTumblr().fetchBlogJson(query);
			JsonParsor jsonParsor = new JsonParsor(jsonData);
			postList = jsonParsor.parseImageLinks(postList);

			query.setStartPost(query.getStartPost() + MAX_LIMIT_TO_FETCH_POST);
		} while (remainingPostsToFetch > 0);

		return postList;
	}
}
