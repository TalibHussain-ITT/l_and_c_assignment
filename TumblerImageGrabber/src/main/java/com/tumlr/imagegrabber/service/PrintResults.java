package com.tumlr.imagegrabber.service;

import static com.tumlr.imagegrabber.util.PrintUtil.printLine;

import org.json.JSONArray;
import org.json.JSONException;

import com.tumlr.imagegrabber.model.TumblrBlog;

public class PrintResults {

	private static final String PRINT_TITLE = "title: ";
	private static final String PRINT_NAME = "name: ";
	private static final String PRINT_DESCRIPTION = "description: ";
	private static final String PRINT_POST_COUNT = "no of post: ";

	public PrintResults() {
		super();
	}

	public void printBlogDetails(TumblrBlog blog) throws JSONException {
		printLine(PRINT_TITLE + blog.getTitle());
		printLine(PRINT_NAME + blog.getName());
		printLine(PRINT_DESCRIPTION + blog.getDescritpion());
		printLine(PRINT_POST_COUNT + blog.getPostCount());
	}

	public void printPhotoLinks(JSONArray postList, int startPost) throws JSONException {
		int postNumber = startPost + 1;

		for (int currentPostIndex = 0; currentPostIndex < postList.length(); currentPostIndex++) {
			JSONArray photoList = postList.getJSONArray(currentPostIndex);
			printLine(postNumber);

			for (int currentImageIndex = 0; currentImageIndex < photoList.length(); currentImageIndex++) {

				// Fetching Current Image Link.
				printLine(photoList.getString(currentImageIndex));
			}
			postNumber++;
		}
	}
}
