package com.tumlr.imagegrabber.service;

import java.util.Scanner;
import com.tumlr.imagegrabber.model.UserQuery;
import static com.tumlr.imagegrabber.util.PrintUtil.printLine;

public class ConsoleInput {

	private static final String BLOGNAME_MESSAGE = "Enter the tumblr blog name :";
	private static final String RANGE_MESSAGE = "Enter the range:";
	private static final String MINIMUM_VALUE_MESSAGE = "Enter Minimum value of Range :";
	private static final String MAXIMUM_VALUE_MESSAGE = "Enter Maximum value of Range :";
	private Scanner consoleInput;

	ConsoleInput() {
		super();
		this.consoleInput = new Scanner(System.in);
	}

	private int takeValidNumber() {
		int number = 0;
		do {
			number = this.consoleInput.nextInt();
		} while (number <= 0);

		return number;
	}

	public UserQuery takeUserInput() {
		printLine(BLOGNAME_MESSAGE);
		String blogName = consoleInput.nextLine();
		int endPost = 0;
		int startPost = 0;
		
		do {
			printLine(RANGE_MESSAGE);
			printLine(MINIMUM_VALUE_MESSAGE);
			startPost = takeValidNumber() - 1;
			printLine(MAXIMUM_VALUE_MESSAGE);
			endPost = takeValidNumber();
		} while (startPost > endPost);

		return new UserQuery(startPost, endPost, blogName);
	}
}
