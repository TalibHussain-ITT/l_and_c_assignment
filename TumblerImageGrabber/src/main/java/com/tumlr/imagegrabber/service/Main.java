package com.tumlr.imagegrabber.service;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;

import com.tumlr.imagegrabber.exceptions.PostOutOfRangeException;
import com.tumlr.imagegrabber.model.TumblrBlog;
import com.tumlr.imagegrabber.model.UserQuery;

public class Main {

	private static final String POST_OUT_OF_RANGE_MESSAGE = "The Current posts are less then requested posts.";

	public static void main(String[] args) throws IOException, JSONException {
		UserQuery query = new ConsoleInput().takeUserInput();
		Tumblr tumblr = new Tumblr();

		// Fetching blog data.
		TumblrBlog blog = tumblr.fetchBlogDetails(query.getBlogName());
		int startPost = query.getStartPost();
		int requestedPost = query.getEndPost() - startPost;
		int currentPostCount = blog.getPostCount();

		if (requestedPost > currentPostCount || startPost > currentPostCount) {
			throw new PostOutOfRangeException(POST_OUT_OF_RANGE_MESSAGE);
		}

		JSONArray postList = tumblr.fetchPostPhotos(query);

		PrintResults reportPrinter = new PrintResults();
		reportPrinter.printBlogDetails(blog);
		reportPrinter.printPhotoLinks(postList, startPost);
	}
}
