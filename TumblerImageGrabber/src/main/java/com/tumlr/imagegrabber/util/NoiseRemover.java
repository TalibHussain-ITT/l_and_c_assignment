package com.tumlr.imagegrabber.util;

public class NoiseRemover {

	private static final int JSON_START_INDEX = 22;

	public static String removeNoise(String rawResponseContent) {

		// JSON Data return in form of JavaScript variable.
		int jsonEndIndex = rawResponseContent.length() - 1;
		String cleanedJson = rawResponseContent.substring(JSON_START_INDEX, jsonEndIndex);
		return cleanedJson;
	}
}
