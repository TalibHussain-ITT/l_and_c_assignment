# Introduction:
___
>Tumblr is an American microblogging and social networking website founded by David Karp in 2007 and currently owned by Automattic. 
The service allows users to post multimedia and other content to a short-form blog.
https://www.tumblr.com/
Tumblr provides API to get info about public blogs. However, there are two versions of API, v1 and v2.
For this assignment, we are going to use API v1.

# Purpose of This project:
___
>To fetch the urls of all the images of quality 1280 with the help of API 
Printing in the following format:
 title:                                                                             
 name: good                                                                         
 description: Live Well. Do Good.                                                   
 no of post: 2182   
 1. https://66.media.tumblr.com/fb7d65b760ddcf6f3293e5a8e4484358/tumblr_on4jrhF3ZT1qjq5r9o1_500.png  
    https://66.media.tumblr.com/e3a2b8f284c2833dcaaf2ff9787f5ef9/tumblr_on4jrhF3ZT1qjq5r9o2_640.png  
 2. https://66.media.tumblr.com/2b2f2113520c4e9ed17c5cdd06ba6c2f/tumblr_obthv5IUes1qjq5r9o1_1280.jpg 
 3. https://66.media.tumblr.com/d494984b74bc191e6b8e99301157f2f2/tumblr_o9nolw7si01qjq5r9o1_1280.jpg 
 4. https://66.media.tumblr.com/ef9ac5b85ed8a70c7f313f913213ef61/tumblr_o9hu3pKRUR1qjq5r9o1_1280.jpg 
 5. https://66.media.tumblr.com/9c528fcb45530501cf100ac2dc50abae/tumblr_o9fx66HdfB1qjq5r9o1_500.gif  


# Formatting for the code
___
> This is to provide a structure to the code. Follow these specific rules to keep the consistency among the team. 

## Naming Convention
>Our project use camel case naming convention for variable,method and Pascal case for class,enum and interfaces and all block with underscore as seperator for constant.
#### Example :
##### Class/Interface/Enum: 
```java
Class TumblrBlog{...........
Interface ParseJson{......
Enum BlogType{..........
```

##### Method: 
```java
int startPost,endPost;
TumblrBlog blog;
```

##### Variable: 
```java
int startPost,endPost;
TumblrBlog blog;
```
>for more details  click [here](https://www.oracle.com/java/technologies/javase/codeconventions-namingconventions.html)

## Package naming strucute for this project.
#### Example :
```java
package com.tumblr.imagegrbber.xyz;
```

## Space between concepts.
>There will a single space between the concepts of file.

#### Example:
##### Incorrect:
```java
package com.tumlr.imagegrabber.service;
import com.tumlr.imagegrabber.model.UserQuery;
public class TumblrURL {......
```
##### Correct:
```java
package com.tumlr.imagegrabber.service;

import com.tumlr.imagegrabber.model.UserQuery;

public class TumblrURL {......
```

## Imports :
* Imports should usually be on separete lines.
* User defined package will be imported after builtin package

##### Incorrect :
```java
import com.tumlr.imagegrabber.exceptions.BlogNotFoundException;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.net.URL;
import java.nio.charset.Charset;
```
##### Correct :
```java
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.net.URL;
import java.nio.charset.Charset;
import com.tumlr.imagegrabber.exceptions.BlogNotFoundException;
```

## Order of declaration :
##### Variable order.
1. Declare the constants .
2. Declare the static variables.
3. Declare the instance variable.

##### Incorrect :
```java
private static Conncection dbConnection;
private Scanner consoleInput;
private final String MAXIMUM_VALUE_MESSAGE = "Enter Maximum value of Range :";
```
##### Correct :
```java
private final String MAXIMUM_VALUE_MESSAGE = "Enter Maximum value of Range :";
private static Conncection dbConnection;
private Scanner consoleInput;
```

## Spaces around parentheses and blocks :
* One space before opening parenthais any parenthesis and one space apfer closing parenthasis.

#### Example
##### Incorrect :
```java
    if(photos != null && photos.length() > 0){
```

##### Correct :
```java
    if (photos != null && photos.length() > 0) {
```
* No space for '[ , ]'.

#### Example
##### Incorrect :
```java
    String args [] ;
```
##### Correct :
```java
    String args[];
```

* No space between opening perenteshis while using in function.

#### Example
##### Incorrect :
```java
    public static String removeNoise (String rawResponseContent) {
        doSomething (param, otherParam) ;
```
##### Correct :
```java
    public static String removeNoise(String rawResponseContent) {
        doSomething(param, otherParam);
```


* Opening braces should start after the end of function, loop or block declaration.
* one space before opening braces and next line opening braces.
* Closing braces should be in new line.

#### Example
##### Incorrect :
```java
    public static void main(String args[]) throws IOException 
    {
         if (photos != null && photos.length() > 0) 
         {
            ............
         }
         ....
         while (x > 0) 
         {
             .........
         }
    } 
```
##### Correct :
```java
    public static void main(String args[]) throws IOException {
         if (photos != null && photos.length() > 0) {
            ............
         }
         ....
         while (x > 0) {
             .........
         }
    } 
```

## Methods vertical ordering
Methods should be ordered in such a way so dependent Method should be above their dependency method.

#### Example
#### Incorrect:
```java
public static bool isValidAsset(Asset asset) {
}
public static void displayAssets(List<Asset> assets) {
}
public static Asset addAsset(Asset asset) {
    if (isValidAsset(asset)) {
    }
}
```
#### Correct:
```java
public static Asset addAsset(Asset asset) {
    if (isValidAsset(asset)) {
    }
}
public static bool isValidAsset(Asset asset) {
}
```

## Levels of Indentation 
* 4 space is provided in function defination
* Go with the levels of indentation

#### Example :
```java
	public JSONArray parseImageLinks(JSONArray postImageList) {
		JSONArray postsJson = this.blogJson.getJSONArray(POSTS);
		for (int postIndex = 0; postIndex < postsJson.length(); postIndex++) {
			JSONArray photos = postsJson.getJSONObject(postIndex);
			if (hasMoreThanOnePhoto(photos)) {
				postImageList.put(parsePhotoLinks(photos));
			} else {
				String photoLink = postsJson.getJSONObject(postIndex);
				JSONArray photoList = new JSONArray().put(photoLink);
				postImageList.put(photoList);
			}

```
## Paramter spacing
* Separation between the parameters in function calling. Provide single space after the argument separator( ***, *** ).

#### Example
##### Incorrect :
```java
    runQuery(startPost,endPost,blogName);
```
##### Correct :
```java
    runQuery(startPost, endPost, blogName);
```

## Maximum Line Length
* Limit all lines to a maximum of 100 characters.

## Blank Lines
* Method inside a class must have a single line space 

#### Example: 
```java
	public String fetchBlogJson(UserQuery query) throws IOException {
		...
	}

	public TumblrBlog fetchBlogDetails(String blogName) throws IOException {
	    ...
	}

	public JSONArray fetchPostPhotos(UserQuery query) throws IOException {
	    ...
	}

```

## Space in operators :
##### Urinary operators :
* There is no space required before or after unrinary operators;

#### Example
##### Incorrect :
```java
    i ++ ;
```
##### Correct :
```java
    i++;
```

##### Urinary operators :
* There is one character space required before and after binary operators;

#### Example
##### Incorrect :
```java
    1. z=x+y;
    2. if(x>=y){
    3. x=y;
```
##### Correct :
```java
    1. z = x + y;
    2. if (x >= y) {
    3. x = y;
```

## Comments
* Comments should be complete sentences. The first word should be capitalized, unless it is an identifier that begins with a lower case letter (never alter the case of identifiers!).
* If single line comment try to add at the end of line or if not then add at abouve the line.
* Add a empty line above comment.
