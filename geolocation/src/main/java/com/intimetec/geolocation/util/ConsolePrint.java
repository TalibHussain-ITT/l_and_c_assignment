package com.intimetec.geolocation.util;

import com.intimetec.geolocation.model.Place;
import com.intimetec.geolocation.model.Places;

public class ConsolePrint {

	private static void printLine(Object object) {
		System.out.println(object.toString());
	}

	public static void printWelcomeMessage() {
		printLine(" +++++++++++++++++++ Welecome to Geolocation Application +++++++++++++++++++");
		printLine("Please enter a place name");
	}

	public static void printPlaces(Places places) {
		printLine("The matched places are : ");
		for (int i = 0; i < places.numberOfPlaces(); i++) {
			printLine("Place number : " + (i+1));
			printPlace(places.getPlace(i));
			printLine("");
		}
	}

	private static void printPlace(Place place) {
		printLine("Place Address : " + place.getAddress());
		printLine("Place latitude : " + place.getLatitude());
		printLine("Place Longitutude : " + place.getLongitude());
	}

}
