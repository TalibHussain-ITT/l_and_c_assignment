package com.intimetec.geolocation.exception;

public class ConfigurationsNotFound extends RuntimeException{

	private static final long serialVersionUID = -2255348552118831909L;

	public ConfigurationsNotFound(String message) {
		super(message);
	}
}
