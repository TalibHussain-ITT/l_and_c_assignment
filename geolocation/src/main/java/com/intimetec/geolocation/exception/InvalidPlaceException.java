package com.intimetec.geolocation.exception;

public class InvalidPlaceException extends RuntimeException {

	private static final long serialVersionUID = -6237068422248070667L;

	public InvalidPlaceException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidPlaceException(String message) {
		super(message);
	}

}
