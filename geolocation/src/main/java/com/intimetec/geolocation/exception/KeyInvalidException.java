package com.intimetec.geolocation.exception;

public class KeyInvalidException extends RuntimeException {

	private static final long serialVersionUID = -2094721561149638795L;

	public KeyInvalidException(String message) {
		super(message);
	}

	public KeyInvalidException(String message, Throwable cause) {
		super(message, cause);
	}

}
