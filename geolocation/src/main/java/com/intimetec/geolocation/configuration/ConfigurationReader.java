package com.intimetec.geolocation.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.intimetec.geolocation.exception.ConfigurationsNotFound;

public class ConfigurationReader {
	private static final String FILE_LOCATION = "config.properties";
	private static final String CONFIGURANTIONS_NOT_FOUND_MESSAGE = "The Required URI configurations not found";

	public Properties fetchConfigurations() {
		Properties properties = null;
		try (InputStream fileInputStream = creatingResource()) {
			properties = new Properties();
			properties.load(fileInputStream);
		} catch (IOException exception) {
			throw new ConfigurationsNotFound(CONFIGURANTIONS_NOT_FOUND_MESSAGE);
		}
		return properties;
	}

	private static InputStream creatingResource() {
		return ConfigurationReader.class.getClassLoader().getResourceAsStream(FILE_LOCATION);
	}
}
