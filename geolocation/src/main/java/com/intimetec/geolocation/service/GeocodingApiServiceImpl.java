package com.intimetec.geolocation.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

import com.intimetec.geolocation.exception.KeyInvalidException;

public class GeocodingApiServiceImpl implements GeocodingApiService {

	private static final String API_KEY_NOT_EXIST_MSG = "The API-Key Does not exist. Unable to connect the server.";
	private static final String ENCODIN_TYPE = "UTF-8";
	private String URI;

	public GeocodingApiServiceImpl() {
		this.URI = new URIService().getURI();
	}

	@Override
	public String makeRequest(String placeName) {
		this.URI = String.format(this.URI, placeName);

		String requestData = null;
		try {
			InputStream rawDataStream = new URL(this.URI).openStream();
			BufferedReader dataReader = new BufferedReader(
					new InputStreamReader(rawDataStream, Charset.forName(ENCODIN_TYPE)));
			requestData = readResponsedData(dataReader);

		} catch (IOException exception) {
			throw new KeyInvalidException(API_KEY_NOT_EXIST_MSG);
		}
		return requestData;
	}

	private String readResponsedData(BufferedReader dataReader) throws IOException {
		StringBuilder responseData = new StringBuilder();
		String currentLine = dataReader.readLine();
		while (currentLine != null) {
			responseData.append(currentLine);
			currentLine = dataReader.readLine();
		}
		return responseData.toString();
	}
}
