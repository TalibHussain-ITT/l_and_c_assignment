package com.intimetec.geolocation.service;

import java.util.Scanner;

import com.intimetec.geolocation.exception.InvalidPlaceException;
import com.intimetec.geolocation.util.InputConverter;

public class UserInput {

	private static final String INVALID_PLACE_NAME_MESSAGE = "The entered place name is invalid";
	private Scanner consoleInput;
	
	public UserInput() {
		this.consoleInput = new Scanner(System.in);
	}
	
	public String takePlaceName() {
		String placeName =  consoleInput.nextLine().trim();
		
		if(placeName.equals("")) {
			throw new InvalidPlaceException(INVALID_PLACE_NAME_MESSAGE);
		}
		
		placeName = InputConverter.convertPlaceInput(placeName);
		return placeName;
	}
	   public static boolean validateAddress(String address) {
	      return address.matches(address);
	   }
}
