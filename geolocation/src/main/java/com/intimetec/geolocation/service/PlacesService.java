package com.intimetec.geolocation.service;

import com.intimetec.geolocation.exception.InvalidPlaceException;
import com.intimetec.geolocation.exception.KeyInvalidException;
import com.intimetec.geolocation.model.Places;
import com.intimetec.geolocation.util.ConsolePrint;

public class PlacesService {

	private GeocodingApiService geocodingApiService;
	private JsonParsor jsonParsor;

	public PlacesService() {
		this.geocodingApiService = new GeocodingApiServiceImpl();
		this.jsonParsor = new JsonParsorImpl();
	}

	public void requestPlaces(String placeName) {
		Places places = null;
		String responseData = this.geocodingApiService.makeRequest(placeName);
		places = this.jsonParsor.parsePlaces(responseData);
		ConsolePrint.printPlaces(places);
	}
}
