package com.intimetec.geolocation.service;

public interface GeocodingApiService {

	String makeRequest(String placeName);

}