package com.intimetec.geolocation.service;

import java.util.Properties;

import com.intimetec.geolocation.configuration.ConfigurationReader;

public class URIService {

	private static final String URI = "uri";
	private static final String API_KEY = "api-key";
	private Properties configerations;

	public URIService() {
		this.configerations = new ConfigurationReader().fetchConfigurations();
	}

	public String getURI() {
		String uri = this.configerations.getProperty(URI);
		String apiKey = this.configerations.getProperty(API_KEY);

		return uri + apiKey;
	}

}
