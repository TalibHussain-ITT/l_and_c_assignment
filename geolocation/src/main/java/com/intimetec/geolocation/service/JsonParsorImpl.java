package com.intimetec.geolocation.service;

import org.json.JSONArray;
import org.json.JSONObject;

import com.intimetec.geolocation.exception.InvalidPlaceException;
import com.intimetec.geolocation.model.Place;
import com.intimetec.geolocation.model.Places;

public class JsonParsorImpl implements JsonParsor {

	private static final String INVALID_PLACE_EXCEPTION_MESSAGE = "The entered place Does not exist. Please enter a valid place.";
	private static final String FEATURES = "features";
	private static final String PLACE_NAME = "place_name";
	private static final String GEOMETRY = "geometry";
	private static final String COORDINATES = "coordinates";
	private static final int LATITUDE_INDEX = 0;
	private static final int LONGITUDE_INDEX = 1;

	@Override
	public Places parsePlaces(String apiResponse) {
		JSONArray jsonPlaces = new JSONObject(apiResponse).getJSONArray(FEATURES);
		Places places = new Places();
		if (jsonPlaces.isEmpty()) {
			throw new InvalidPlaceException(INVALID_PLACE_EXCEPTION_MESSAGE);
		} else {
			for (int i = 0; i < jsonPlaces.length(); i++) {
				Place place = parsePlace(jsonPlaces.getJSONObject(i));
				places.addPlace(place);
			}
		}
		return places;
	}

	private Place parsePlace(JSONObject jsonPlace) {
		String address = jsonPlace.getString(PLACE_NAME);
		JSONArray coordinates = jsonPlace.getJSONObject(GEOMETRY).getJSONArray(COORDINATES);
		String latitude = coordinates.get(LATITUDE_INDEX).toString();
		String longitude = coordinates.get(LONGITUDE_INDEX).toString();

		return new Place(address, latitude, longitude);
	}

}
