package com.intimetec.geolocation.service;

import com.intimetec.geolocation.exception.InvalidPlaceException;
import com.intimetec.geolocation.exception.KeyInvalidException;
import com.intimetec.geolocation.util.ConsolePrint;

public class Main {

	public static void main(String[] args) {
		while (true) {
			runApplication();
		}
	}

	private static void runApplication() {
		ConsolePrint.printWelcomeMessage();
		try {
			String placeName = new UserInput().takePlaceName();
			new PlacesService().requestPlaces(placeName);
		} catch (InvalidPlaceException | KeyInvalidException exception) {
			System.out.println(exception.getMessage());
		}
	}
}
