package com.intimetec.geolocation.service;

import com.intimetec.geolocation.model.Places;

public interface JsonParsor {

	Places parsePlaces(String apiResponse);

}