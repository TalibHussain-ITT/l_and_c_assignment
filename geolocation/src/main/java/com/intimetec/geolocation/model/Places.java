package com.intimetec.geolocation.model;

import java.util.ArrayList;

import com.intimetec.geolocation.model.Place;

public class Places {

	private ArrayList<Place> places;

	public Places() {
		this.places = new ArrayList<>();
	}

	public void addPlace(Place place) {
		this.places.add(place);
	}

	public Place getPlace(int index) {
		return this.places.get(index);
	}

	public int numberOfPlaces() {
		return this.places.size();
	}

}
