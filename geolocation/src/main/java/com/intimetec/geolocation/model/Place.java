package com.intimetec.geolocation.model;

public class Place {
	private String address;
	private String latitude;
	private String longitude;

	public Place(String address, String latitude, String longitude) {
		super();
		this.address = address;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}
}
