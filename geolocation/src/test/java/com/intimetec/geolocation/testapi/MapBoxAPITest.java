package com.intimetec.geolocation.testapi;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MapBoxAPITest {

	private static final String URI = "https://api.mapbox.com/geocoding/v5/mapbox.places/Amrapali%20Circle.json?access_token=pk.eyJ1IjoidmlrcmFtNzAwIiwiYSI6ImNrY2h2MXM2YjA4cGgyc3A1a2U2cHlyMGwifQ.j8LXhJiwRTJe4fg85waLIw";

	private static JSONObject responseJson;

	private static String readResponsedData(BufferedReader dataReader) throws IOException {
		StringBuilder responseData = new StringBuilder();
		String currentLine = dataReader.readLine();

		while (currentLine != null) {
			responseData.append(currentLine);
			currentLine = dataReader.readLine();
		}
		return responseData.toString();
	}

	public void testMapBoxResponse(String uri) throws IOException {
		InputStream rawDataStream = new URL(uri).openStream();
		BufferedReader dataReader = new BufferedReader(new InputStreamReader(rawDataStream, Charset.forName("UTF-8")));
		responseJson = new JSONObject(readResponsedData(dataReader));
	}

	@Test
	@DisplayName("Match the Adress from the response")
	public void testAdress() throws IOException {
		testMapBoxResponse(URI);
		String adress = responseJson.getJSONArray("features").getJSONObject(0).getString("place_name");
		assertEquals("Amrapali Circle, Jaipur, Jaipur, Rajasthan, India", adress);
	}

	@Test
	@DisplayName("Api Key error")
	public void testApiKey() {
		final String incorrectKeyUri = "https://api.mapbox.com/geocoding/v5/mapbox.places/Amrapali%20Circle.json?access_token=fgkdgmfsmd";
		assertThrows(IOException.class,()->{testMapBoxResponse(incorrectKeyUri);});
	}
	
	@Test
	@DisplayName("Match the Adress from the response")
	public void testCoordinates() throws IOException {
		testMapBoxResponse(URI);
		String coodinates = responseJson.getJSONArray("features").getJSONObject(0).getJSONObject("geometry").getJSONArray("coordinates").toString();
		assertEquals("[75.728978,26.895259]", coodinates);
	}

}
