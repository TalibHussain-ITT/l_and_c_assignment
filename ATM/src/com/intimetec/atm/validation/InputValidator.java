package com.intimetec.atm.validation;

import com.intimetec.atm.exception.AmountExceedPerTransaction;
import com.intimetec.atm.exception.AmountNotInDenominationException;
import com.intimetec.atm.exception.InvalidCardNumberException;
import com.intimetec.atm.exception.InvalidPinInputException;

public class InputValidator {

	private static final long CARD_NUMBER_LOWER_LIMIT = 1000000000000000L;
	private static final long CARD_NUMBER_UPPER_LIMIT = 10000000000000000L;
	private static final int PIN_LOWER_LIMIT = 1000;
	private static final int PIN_UPPER_LIMIT = 10000;
	private static final String INPUT_CARD_MESSAGE = "Please enter appropriate card number";
	private static final String INPUT_PIN_MESSAGE = "please enter appropriate pin number";
	private static final String DENOMINATION_MESSAGE = "Please Enter amount in multiple of 100.";
	private static final int DENOMINATION_FACTOR = 100;
	private static final int MAX_DEBIT_LIMIT = 1000;
	private static final String AMOUNT_EXCEED_PER_TRANSACTION_MESSAGE = "Please enter amount below 10000.";

	public void validateCardNumberFormat(long cardNumber) {
		if (isCardNumberNotInLimit(cardNumber)) {
			throw new InvalidCardNumberException(INPUT_CARD_MESSAGE);
		}
	}

	private boolean isCardNumberNotInLimit(long cardNumber) {
		return !(cardNumber > CARD_NUMBER_LOWER_LIMIT && cardNumber <= CARD_NUMBER_UPPER_LIMIT);
	}

	public void validatePinNumberFormat(int pin) {
		if (isPinNumberNotInLimit(pin)) {
			throw new InvalidPinInputException(INPUT_PIN_MESSAGE);
		}
	}

	private boolean isPinNumberNotInLimit(int pin) {
		return !(pin > PIN_LOWER_LIMIT && pin <= PIN_UPPER_LIMIT);
	}

	public void validateWithdrawAmount(int amount) {
		if (isAmountNotInDenomiation(amount)) {
			throw new AmountNotInDenominationException(DENOMINATION_MESSAGE);
		} else if ( isAmountNotInLimit(amount) ) {
			throw new AmountExceedPerTransaction(AMOUNT_EXCEED_PER_TRANSACTION_MESSAGE);
		}
	}

	private boolean isAmountNotInLimit(int amount) {
		return !( amount > MAX_DEBIT_LIMIT );
	}

	private boolean isAmountNotInDenomiation(int amount) {
		return !((amount % DENOMINATION_FACTOR == 0 ));
	}
}
