package com.intimetec.atm.exception;

public class InvalidCardNumberException extends InvalidInputException {

	private static final long serialVersionUID = 4663695048271811917L;

	public InvalidCardNumberException(String message) {
		super(message);
	}

}
