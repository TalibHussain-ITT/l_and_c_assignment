package com.intimetec.atm.exception;

public class AmountExceedPerTransaction extends InvalidInputException {

	private static final long serialVersionUID = -8322785531009140188L;

	public AmountExceedPerTransaction(String message) {
		super(message);
	}

}
