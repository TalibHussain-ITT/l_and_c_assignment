package com.intimetec.atm.exception;

public class InvalidCardException extends InvalidInputException {

	private static final long serialVersionUID = 2822042486022296676L;

	public InvalidCardException(String message) {
		super(message);
	}

}
