package com.intimetec.atm.exception;

public class ServerException extends RuntimeException {

	private static final long serialVersionUID = -3764734355453454459L;

	public ServerException(String message) {
		super(message);
	}

}
