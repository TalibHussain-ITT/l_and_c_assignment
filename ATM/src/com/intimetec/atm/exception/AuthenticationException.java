package com.intimetec.atm.exception;

public class AuthenticationException extends ServerException {

	private static final long serialVersionUID = -3337239396341924686L;

	public AuthenticationException(String message) {
		super(message);
	}

}
