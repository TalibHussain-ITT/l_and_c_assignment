package com.intimetec.atm.exception;

public class InvalidPinInputException extends InvalidInputException {
 
	private static final long serialVersionUID = 440458866483337526L;

	public InvalidPinInputException(String message) {
		super(message);
	}

}
