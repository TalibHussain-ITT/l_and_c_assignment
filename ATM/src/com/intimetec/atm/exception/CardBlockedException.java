package com.intimetec.atm.exception;

public class CardBlockedException extends ATMException {
 
	private static final long serialVersionUID = 4858973574815403895L;

	public CardBlockedException(String message) {
		super(message);
	}	
}
