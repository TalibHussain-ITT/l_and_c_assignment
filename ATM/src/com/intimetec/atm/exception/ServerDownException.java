package com.intimetec.atm.exception;

public class ServerDownException extends ATMException {

	private static final long serialVersionUID = -1460378640979919571L;

	public ServerDownException(String message) {
		super(message);
	}

}
