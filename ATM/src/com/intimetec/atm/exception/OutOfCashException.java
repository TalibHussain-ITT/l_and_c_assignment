package com.intimetec.atm.exception;

public class OutOfCashException extends ATMException {

	private static final long serialVersionUID = 7807373839702627045L;

	public OutOfCashException(String message) {
		super(message);
	}
	
}
