package com.intimetec.atm.exception;

public class InvalidCustomerException extends ATMException {

	private static final long serialVersionUID = -150393895205394347L;

	public InvalidCustomerException(String message) {
		super(message);
	}
}
