package com.intimetec.atm.exception;

public class InvalidInputException extends RuntimeException {

	private static final long serialVersionUID = -3010657094347491365L;

	public InvalidInputException(String message) {
		super(message);
	}

}
