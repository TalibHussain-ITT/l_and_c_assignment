package com.intimetec.atm.exception;

public class AmountNotInDenominationException extends ATMException {

	private static final long serialVersionUID = -94941632551851969L;

	public AmountNotInDenominationException(String message) {
		super(message);
	}

}
