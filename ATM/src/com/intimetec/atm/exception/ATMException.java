package com.intimetec.atm.exception;

public class ATMException extends RuntimeException {

	private static final long serialVersionUID = -4922978543563716069L;

	public ATMException(String message) {
		super(message);
	}

	public ATMException(String message, Throwable cause) {
		super(message, cause);
	}

}
