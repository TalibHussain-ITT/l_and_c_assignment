package com.intimetec.atm.exception;

public class LowBalanceException extends ATMException {

	private static final long serialVersionUID = 4799411575720945752L;

	public LowBalanceException(String message) {
		super(message);
	}
	
}
