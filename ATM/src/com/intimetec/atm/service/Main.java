package com.intimetec.atm.service;

import java.util.Scanner;

import com.intimetec.atm.util.ConsolePrinter;

public class Main {
	public static void main(String args[]) {
		ATMService atmService = ATMInitialiser.initialize();
		while (true) {
			try {
				new ServerConnection().connectServer();
				ConsolePrinter.printWelcomeMessage();
				new ATMInterface().operations(atmService);
			} catch (Exception exception) {
				System.out.println(exception.getMessage());
			}
		}
	}

}
