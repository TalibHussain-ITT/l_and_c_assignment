package com.intimetec.atm.service;

import com.intimetec.atm.util.ConsolePrinter;

public class ATMInterface {

	private static final String CHECK_BALANCE_MESSAGE = "Your balance is";

	public void operations(ATMService atmService) {

		ConsoleInput consoleInput = new ConsoleInput();

		long cardNumber = consoleInput.enterCardNumber();
		int pin = consoleInput.enterPinNumber();
		atmService.validateCustomer(cardNumber, pin);
		ConsolePrinter.printOptions();
		performOperations(consoleInput, atmService, cardNumber);

	}

	private void performOperations(ConsoleInput consoleInput, ATMService atmService, long cardNumber) {

		int choice = consoleInput.enterChoice();

		switch (choice) {
		case 1:
			int amount = consoleInput.enterAmount();
			atmService.withdrawBalance(amount, cardNumber);
			break;
		case 2:
			System.out.println(CHECK_BALANCE_MESSAGE + atmService.checkBalance(cardNumber));
			break;
		}
	}

}
