package com.intimetec.atm.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.intimetec.atm.exception.AuthenticationException;
import com.intimetec.atm.exception.CardBlockedException;
import com.intimetec.atm.exception.InvalidCardException;
import com.intimetec.atm.exception.OutOfCashException;
import com.intimetec.atm.model.ATM;
import com.intimetec.atm.model.Customer;

public class ATMService {

	private static final String CARD_BLOCK_MESSAGE = "The card is blocked";
	private static final String INVALID_PIN_MESSAGE = "The user Invalid Pin Number";
	private static final String INVALID_CARD_MESSAGE = "Plase enter a valid card.";
	private static final String TRANSACTION_COMPLETE_MESSAGE = "Transaction has been completed";
	private static final String INSUFFICIENT_CASH_MESSAGE = "There is no cash is ATM.";
	public HashMap<Long, Integer> customersPins;
	public ArrayList<Customer> customers;
	public ATM atm;

	public ATMService(HashMap<Long, Integer> customersPins, ArrayList<Customer> customers, ATM atm) {
		super();
		this.customersPins = customersPins;
		this.customers = customers;
		this.atm = atm;
	}

	public int checkBalance(long cardNumber) {
		Customer customer = findCustomerByCard(cardNumber);
		return customer.getBalance();
	}

	public void withdrawBalance(int amount, long cardNumber) {
		if (isSufficientCashAvailable(amount)) {
			throw new OutOfCashException(INSUFFICIENT_CASH_MESSAGE);
		} else {
			Customer customer = findCustomerByCard(cardNumber);
			customer.debitMoney(amount);
			System.out.println(TRANSACTION_COMPLETE_MESSAGE);
		}
	}

	private boolean isSufficientCashAvailable(int amount) {
		return (amount > atm.getCash());
	}

	public void validateCustomer(long cardNumber, int pin) {
		if (isCardValid(cardNumber)) {
			
			Customer customer = findCustomerByCard(cardNumber);
			checkForCardBlock(customer);
			validatePin(cardNumber, pin, customer);
		
		} else {
			throw new InvalidCardException(INVALID_CARD_MESSAGE);
		}
	}

	private void validatePin(long cardNumber, int pin, Customer customer) {
		if (this.customersPins.get(cardNumber) != pin) {
			customer.decreaseAttempt();
			throw new AuthenticationException(INVALID_PIN_MESSAGE);
		}
	}

	private boolean isCardValid(long cardNumber) {
		return this.customersPins.containsKey(cardNumber);
	}

	private void checkForCardBlock(Customer customer) {
		if (customer.isCardBlocked()) {
			throw new CardBlockedException(CARD_BLOCK_MESSAGE);
		}
	}

	private Customer findCustomerByCard(long cardNumber) {
		Customer customer = null;
		for (Customer cust : this.customers) {
			if (cust.getCardNumber() == cardNumber) {
				customer = cust;
			}
		}
		return customer;
	}
}
