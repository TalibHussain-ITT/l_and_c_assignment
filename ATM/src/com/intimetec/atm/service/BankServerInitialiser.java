package com.intimetec.atm.service;

import java.io.IOException;
import java.net.ServerSocket;

import com.intimetec.atm.exception.ServerException;

public class BankServerInitialiser {

	private static final String WELCOME_SERVER_MESSAGE = "Welcome to the bank server";
	private static final String SERVER_EXCEPTION_MESSAGE = "Unable to start server.";

	public static void main(String[] args) {
		ServerSocket serverSocket;
		try {
			System.out.println(WELCOME_SERVER_MESSAGE);
			serverSocket = new ServerSocket(6666);
			while (true) {
				serverSocket.accept();
			}
		} catch (IOException exception) {
			throw new  ServerException(SERVER_EXCEPTION_MESSAGE);
		}
	}
}
