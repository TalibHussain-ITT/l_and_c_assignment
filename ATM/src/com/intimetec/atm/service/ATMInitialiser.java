package com.intimetec.atm.service;

import java.util.ArrayList;
import java.util.HashMap;

import com.intimetec.atm.model.ATM;
import com.intimetec.atm.model.Account;
import com.intimetec.atm.model.Customer;
import com.intimetec.atm.model.DebitCard;

public class ATMInitialiser {
	
	private static final String ATM_NAME = "XYZ";
	private static final String CUSTOMER_NAME = "Alpha";

	public static ATMService initialize() {
		ATM atm = new ATM(ATM_NAME, 1000);
		HashMap<Long, Integer> customersPins = new HashMap<>();
		customersPins.put(1234567890123456L, 1234);
		Account account = new Account(123456789L, 1200);
		DebitCard card = new DebitCard(1234567890123456L, 1234);
		Customer customer = new Customer(CUSTOMER_NAME, account, card);
		ArrayList<Customer> customers = new ArrayList<>();
		customers.add(customer);
		ATMService atmService = new ATMService(customersPins, customers, atm);
		return atmService;
	}
}
