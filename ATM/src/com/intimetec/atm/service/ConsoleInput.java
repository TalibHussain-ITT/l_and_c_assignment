package com.intimetec.atm.service;

import java.util.Scanner;

import com.intimetec.atm.validation.InputValidator;

public class ConsoleInput {
	private static final String ENTER_CARD_NUMBER_MESSAGE = "Enter your Card Number";
	private static final String ENTER_PIN_MESSAGE = "Enter your Pin Number";
	private static final String ENTER_DEBIT_AMOUNT_MESSAGE = "Enter Debit Amount";
	private static final String ENTER_CHOICE_MESSAGE = "Enter your choice";
	private Scanner inputScanner;
	private InputValidator inputValidator;

	public ConsoleInput() {
		inputScanner = new Scanner(System.in);
		inputValidator = new InputValidator();
	}

	public long enterCardNumber() {
		System.out.println(ENTER_CARD_NUMBER_MESSAGE);
		long cardNumber = inputScanner.nextLong();
		inputValidator.validateCardNumberFormat(cardNumber);
		return cardNumber;
	}

	public int enterPinNumber() {
		System.out.println(ENTER_PIN_MESSAGE);
		int pin = inputScanner.nextInt();
		inputValidator.validatePinNumberFormat(pin);
		return pin;
	}

	public int enterAmount() {
		System.out.println(ENTER_DEBIT_AMOUNT_MESSAGE);
		int amount = inputScanner.nextInt();
		inputValidator.validateWithdrawAmount(amount);
		return amount;
	}

	public int enterChoice() {
		System.out.println(ENTER_CHOICE_MESSAGE);
		int choice = inputScanner.nextInt();
		return choice;
	}

}
