package com.intimetec.atm.service;

import java.io.IOException;
import java.net.Socket;

import com.intimetec.atm.exception.ServerDownException;

public class ServerConnection {
	
	private static final String SERVER_IP = "localhost";
	private static final String SERVER_DOWN_MESSAGE = "The server have temporarily been down.";

	public void connectServer() {
		try {
			Socket socket = new Socket(SERVER_IP, 6666);
		} catch (IOException e) {
			throw new ServerDownException(SERVER_DOWN_MESSAGE);
		}
	}
}
