package com.intimetec.atm.model;

public class ATM {
	private String location;
	private int cash;

	public ATM(String location, int cash) {
		super();
		this.location = location;
		this.cash = cash;
	}

	public String getLocation() {
		return location;
	}

	public int getCash() {
		return cash;
	}

}
