package com.intimetec.atm.model;

import com.intimetec.atm.exception.LowBalanceException;

public class Customer {
	private String name;
	private long mobileNumber;
	private String adress;
	private String accountType;
	private Account account;
	private DebitCard card;

	public Customer(String name, Account account, DebitCard card) {
		super();
		this.name = name;
		this.account = account;
		this.card = card;
	}

	public long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getName() {
		return name;
	}

	public long getAccountNumber() {
		return this.account.getAccountNumber();
	}

	public long getCardNumber( ) {
		return this.card.getCardNumber();
	}
	
	public boolean isCardBlocked() {
		return this.card.isCardBlocked();
	}
	
	public void decreaseAttempt() {
		this.card.subAttempts();
	}

	public void debitMoney(int amount) {
		if (amount > this.account.getBalance()) {
			throw new LowBalanceException("Low Balance.");
		} else {
			this.account.debitBalance(amount);
		}
	}

	public int getBalance() {
		return this.account.getBalance();
	}
}
