package com.intimetec.atm.model;

public class Account {

	private long accountNumber;
	private int balance;

	public Account(long accountNumber, int balance) {
		super();
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public int getBalance() {
		return balance;
	}

	public void creditBalance(int amount) {
		this.balance += amount;
	}

	public void debitBalance(int amount) {
		this.balance -= amount;
	}

}
