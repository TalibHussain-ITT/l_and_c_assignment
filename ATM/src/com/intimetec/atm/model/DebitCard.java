package com.intimetec.atm.model;

public class DebitCard {

	private static final int MAX_ATTEMPTS = 3;

	private long cardNumber;
	private int cvv;
	private boolean cardBlocked;
	private int attempts;

	public DebitCard(long cardNumber, int cvv) {
		super();
		this.cardNumber = cardNumber;
		this.cvv = cvv;
		this.cardBlocked = false;
		this.attempts = MAX_ATTEMPTS;

	}

	public long getCardNumber() {
		return cardNumber;
	}

	public boolean isCardBlocked() {
		return cardBlocked;
	}

	public void setCardBlocked(boolean cardBlocked) {
		this.cardBlocked = cardBlocked;
	}

	public void subAttempts() {
		this.attempts--;
		if (this.attempts <= 0) {
			this.cardBlocked = true;
		}
	}

	public int getCvv() {
		return cvv;
	}

}
