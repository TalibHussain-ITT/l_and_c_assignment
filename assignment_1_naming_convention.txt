




						Assignment-1: Naming Conventions

			//*******************************************************************************//


Assignment 1: The below program is to Roll the Dice


import random

def get_random_number(upper_limit):
    LOWER_LIMIT=1
    generated_random_number=random.randint(LOWER_LIMIT, upper_limit)
    return generated_random_number


def main():
    MAX_OUTCOME=6
    roll_again=True
    while roll_again:
        users_choice=input("Ready to roll? Enter Q to Quit")
        if users_choice.lower() !="q":
            rolled_outcome = get_random_number(MAX_OUTCOME)
            print("You have rolled a",rolled_outcome)
        else:
            roll_again=False


//*******************************************************************************//


Assignment 3: The below program is to guess the correct number between 1 to 100

def is_guess_valid(user_input):
    if user_input.isdigit() and 1<= int(user_input) <=100:
        return True
    else:
        return False

def main():
    LOWER_LIMIT=1
    UPPER_LIMIT=100
    random_digit=random.randint(LOWER_LIMIT,UPPER_LIMIT)
    guessed_correct=False
    user_guess=input("Guess a number between 1 and 100:")
    guess_count=0
    while not guessed_correct:
        if not is_guess_valid(user_guess):
            user_guess=input("I wont count this one Please enter a number between 1 to 100")
            continue
        else:
            guess_count+=1
            user_guess=int(user_guess)

        if user_guess<random_digit:
            user_guess=input("Too low. Guess again")
        elif user_guess>random_digit:
            user_guess=input("Too High. Guess again")
        else:
            print("You guessed it in",guess_count,"guesses!")
            guessed_correct=True


main()


//*******************************************************************************//


Assignment 4: The below program is to check whether the number is Armstrong number or not


def get_digits_sum_for_armstrong(number_to_check):
    # Initializing Sum and Number of Digits
    digit_sum = 0
    digit_count = 0

    # Calculating Number of individual digits
    remaining_number = number_to_check
    while remaining_number > 0:
        digit_count = digit_count + 1
        remaining_number = remaining_number // 10

    # Finding Armstrong Number
    remaining_number = number_to_check
    for i in range(1, remaining_number + 1):
        remainder = remaining_number % 10
        digit_sum += (remainder ** digit_count)
        remaining_number //= 10
    return digit_sum


# End of Function

# User Input
user_input = int(input("\nPlease Enter the Number to Check for Armstrong: "))

if (user_input == get_digits_sum_for_armstrong(user_input)):
    print("\n %d is Armstrong Number.\n" % user_input)
else:
    print("\n %d is Not a Armstrong Number.\n" % user_input)

//*******************************************************************************//

Assignment 5: Selection Sort

Selection Sort

function sort(arrayToSort) {
  for (let currentIndex = 0; currentIndex < arrayToSort.length; currentIndex++) {
    // Set default active minimum to current index.
    let activeMinIndex = currentIndex;
    // Loop to array from the current value.
    for (let newMinIndex = currentIndex + 1; newMinIndex < arrayToSort.length; newMinIndex++) {
      // If you find an item smaller than the current active minimum,
      // make the new item the new active minimum.
      if (arrayToSort[newMinIndex] < arrayToSort[activeMinIndex]) {
        activeMinIndex = newMinIndex;
      }
      // Keep on looping until you've looped over all the items in the array
      // in order to find values smaller than the current active minimum.
    }
    // If the current index isn't equal to the active minimum value's index anymore
    // swap these two elements.
    if (currentIndex !== activeMinIndex) {
      [arrayToSort[currentIndex], arrayToSort[activeMinIndex]] = [arrayToSort[activeMinIndex], arrayToSort[currentIndex]];
    }
  }
  return arrayToSort;
}

Assignment 6: Find largest and smallest number from an array

Program to find largest and smallest number from an array

import java.util.Arrays;
/**
 * Java program to find largest and smallest number from an array.
 */
public class ArrayMinMax{

    public static void main(String args[]) {
        printArraysMinMax(new int[]{-20, 34, 21, -87, 92,
                             Integer.MAX_VALUE});
        printArraysMinMax(new int[]{10, Integer.MIN_VALUE, -2});
        printArraysMinMax(new int[]{Integer.MAX_VALUE, 40,
                             Integer.MAX_VALUE});
        printArraysMinMax(new int[]{1, -1, 0});
    }

    public static void printArraysMinMax(int[] numbers) {
        int maximum = Integer.MIN_VALUE;
        intsss minimum = Integer.MAX_VALUE;
        for (int number : numbers) {
            if (number > maximum) {
                maximum = number;
            } else if (number < minimum) {
                minimum = number;
            }
        }

        System.out.println("Given integer array : " + Arrays.toString(numbers));
        System.out.println("Largest number in array is : " + maximum);
        System.out.println("Smallest number in array is : " + minimum);
    }
}

//*******************************************************************************//


Assignment 6: Find the floor of the expected value(mean) of the subarray from Left to Right.

Find the floor of the expected value(mean) of the subarray from Left to Right.

You are given an array of n numbers and q queries. For each query you have to print the floor of the expected value(mean) of the subarray from L to R.
Inputs
First line contains two integers N and Q denoting number of array elements and number of queries.
Next line contains N space seperated integers denoting array elements.
Next Q lines contain two integers L and R(indices of the array).
Output
print a single integer denoting the answer.
*/
using System;
using System.Numerics;
class FloorOfSubarrayMean {
    static void Main(string[] args) {
       var numOfElementsAndQueries = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            var elements = Array.ConvertAll(Console.ReadLine().Split(' '), long.Parse);
            long[]  = new long[numOfElementsAndQueries[0] + 1];
            prefixSum[0] = 0;
            for (int i = 1; i <= numOfElementsAndQueries[0]; i++)
            {
                prefixSum[i] = prefixSum[i - 1] + elements[i - 1];
            }
            for (var x = 0; x < numOfElementsAndQueries[1]; x++)
            {
                var indicesRange = Array.ConvertAll(Console.ReadLine().Split(' '), int.
                Console.WriteLine((long)((long)(prefixSum[indicesRange[1]] - prefixSum[indicesRange[0] - 1]) / (indicesRange[1] - indicesRange[0] + 1)));
            }
    }
}

/* Example
Input
5 3
1 2 3 4 5
1 3
2 4
2 5
Output2
3
3*/

//*******************************************************************************//
