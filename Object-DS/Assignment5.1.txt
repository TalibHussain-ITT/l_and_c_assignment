Class Employee {
	string name;
	int age;
	float salary;
	public: string getName();
	void setName(string name);
	int getAge();
	void setAge(int age);
	float getSalary();
	void setSalary(float salary);
};
Employee employee;
Que. Is 'employee'an object or a data structure ? Why ?
Ans : It is a data strucutre because:
	1. Employee class just have private fields and we can access them with the help of accessors and mutators(gettor & settors). 
	2. By looking at its functions we can tell that these are concrete terms and,
	   as we have learned in clean code book the concrete terms are used for creating data sturcures. 
	3. There are no meaningful functions i.e.,  It is not exhibiting any behaviour other than acessors/mutators.  
