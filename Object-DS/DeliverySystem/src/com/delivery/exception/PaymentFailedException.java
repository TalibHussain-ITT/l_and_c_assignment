package com.delivery.exception;

public class PaymentFailedException extends DebitFailedException {

	private static final long serialVersionUID = 7217630277307340573L;

	public PaymentFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public PaymentFailedException(String message) {
		super(message);
	}

}
