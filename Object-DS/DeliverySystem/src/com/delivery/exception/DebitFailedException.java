package com.delivery.exception;

public class DebitFailedException extends RuntimeException {

	private static final long serialVersionUID = -4173098779409408948L;

	public DebitFailedException(String message, Throwable cause) {
		super(message, cause);
	}

	public DebitFailedException(String message) {
		super(message);
	}
	
}
