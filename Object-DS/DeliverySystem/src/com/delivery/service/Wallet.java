package com.delivery.service;

import com.delivery.exception.DebitFailedException;

public class Wallet {

	private static final String DEBIT_FAIL_MESSAGE = "The amount is not present.";
	
	// In real application the balance will be coming from DB.
	private double balance = 20.0;

	public double getBalance() {
		return balance;
	}

	public void depositMoney(double depositAmount) {
		balance += depositAmount;
	}

	public void debitMoney(double billAmount) {

		if (billAmount < balance) {
			balance -= billAmount;
		} else {
			throw new DebitFailedException(DEBIT_FAIL_MESSAGE);
		}
	}
}