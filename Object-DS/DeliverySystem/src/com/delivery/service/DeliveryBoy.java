package com.delivery.service;

import com.delivery.exception.PaymentFailedException;
import com.delivery.model.Person;

public class DeliveryBoy extends Person {

	private static final String PAYMENT_SUCCES_MESSAGE = "Payment Successful";

	public DeliveryBoy(String firstName, String lastName) {
		super(firstName, lastName);
	}

	public void getPayment(Customer customer, double billAmount) {
		try {
			customer.doPayment(billAmount);
			System.out.println(PAYMENT_SUCCES_MESSAGE);
		} catch (PaymentFailedException exception) {
			System.out.println(exception.getMessage());
		}
	}
}
