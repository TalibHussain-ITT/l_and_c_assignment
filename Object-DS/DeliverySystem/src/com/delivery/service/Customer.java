package com.delivery.service;

import com.delivery.exception.PaymentFailedException;
import com.delivery.model.Person;

public class Customer extends Person {

	private static final String WALLET_EXCEPTION_MESSAGE = "Either customer wallet is not avilable or amount in wallet is not available ";
	private Wallet myWallet;

	public Customer(String firstName, String lastName) {
		super(firstName, lastName);
		myWallet = new Wallet(); // Will get wallet details from DB.
	}

	public void doPayment(double billAmount) {

		if (isWalletAvailable() && isAmountAvailable(billAmount)) {
			myWallet.debitMoney(billAmount);
		} else {
			throw new PaymentFailedException(WALLET_EXCEPTION_MESSAGE);
		}
	}

	private boolean isAmountAvailable(double billAmount) {
		return (myWallet.getBalance() > billAmount);
	}

	private boolean isWalletAvailable() {
		return (myWallet != null);
	}
}
