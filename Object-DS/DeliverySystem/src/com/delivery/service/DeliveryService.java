package com.delivery.service;

import java.util.Scanner;

public class DeliveryService {

	private static final String ENTER_BILL_AMOUNT_MESSAGE = "Please Enter the Bill Amount for Customer : " ;
	private static final String WELCOME_MESSAGE = "Welcome : ";

	public static void main(String[] args) {

		Scanner consoleInput = new Scanner(System.in);
		Customer customer = new Customer("Jhon", "Doe");
		DeliveryBoy deliveryBoy = new DeliveryBoy("Luke", "Skywalker");

		System.out.println(WELCOME_MESSAGE + deliveryBoy.toString());
		System.out.println(ENTER_BILL_AMOUNT_MESSAGE+ customer.toString());

		double billAmount = consoleInput.nextDouble();
		deliveryBoy.getPayment(customer, billAmount);
		
		consoleInput.close();
	}
}
